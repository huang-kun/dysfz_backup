
from sqlalchemy import Column, String, Integer, BigInteger, Date, Float, create_engine, ForeignKey
from sqlalchemy.orm import sessionmaker, relationship, backref
from sqlalchemy.ext.associationproxy import association_proxy
from sqlalchemy.ext.declarative import declarative_base

import datetime


Base = declarative_base()
cached_tags = []


class Movie(Base):
    __tablename__ = 'movies'

    id = Column(BigInteger, primary_key=True, autoincrement=True)
    name = Column(String(50))
    date = Column(Date, nullable=True)
    description = Column(String(200), nullable=True)
    visits = Column(Integer, nullable=True)
    doubanScore = Column(Float, nullable=True)

    tags = relationship('MovieTag')

    def __init__(self, name, date, visits, description=None, doubanScore=None):
        self.name = name
        self.date = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        self.visits = visits
        self.description = description
        self.doubanScore = doubanScore


class Tag(Base):
    __tablename__ = 'tags'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(20))

    def __init__(self, name):
        self.name = name


class MovieTag(Base):
    __tablename__ = 'movie2tag'

    movieId = Column(BigInteger, ForeignKey('movies.id'), primary_key=True)
    tagId = Column(Integer, ForeignKey('tags.id'), primary_key=True)

    movie = relationship(Movie)
    tag = relationship(Tag)

    def __init__(self, movie=None, tag=None):
        self.movie = movie
        self.tag = tag


# 初始化数据库连接:
engine = create_engine('mysql://root@localhost:3306/dysfz')
# 创建DBSession类型:
DBSession = sessionmaker(bind=engine)


def take_tags(tagnames, session):
    tmp_tags = []
    tmp_names = []
    has_new = False

    for tagname in tagnames:
        for cached_tag in cached_tags:
            if cached_tag.name == tagname:
                tmp_tags.append(cached_tag)
                tmp_names.append(tagname)
                break

    for tagname in tagnames:
        if tagname not in tmp_names:
            tag = session.query(Tag).filter(Tag.name==tagname).one_or_none()
            if tag is None:
                has_new = True
                tag = Tag(name=tagname)
                session.add(tag)
            tmp_tags.append(tag)
            cached_tags.append(tag)

    # if has_new:
    #     session.commit()

    return tmp_tags

if __name__ == '__main__':
    session = DBSession()
    movie = Movie(name='Chinese Animals', date='2018-08-16', visits=25849, doubanScore=8.8)
    for tag in take_tags(['hello', 'China'], session):
        mt = MovieTag(movie=movie, tag=tag)
        movie.tags.append(mt)
    session.add(movie)
    session.commit()
    session.close()