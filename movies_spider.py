
import scrapy
import re

class MoviesSpider(scrapy.Spider):
    name = "movies"

    start_urls = [
        'http://www.dysfz.cc',
    ]

    # text to left bracket
    re_ttlb = re.compile(r"^.+?(?=【)")

    def parse(self, response):
        mls = response.css('ul.movie-list li')

        for m in mls:
            title = m.css('h2 a::text').extract_first()
            title = self.trim_title(title)
            description = m.css('div.txt::text').extract_first()
            doubanScore = m.css('span.dbscore b::text').extract_first()
            
            info = m.css('div.txt p')
            releaseDate = info.xpath('span/text()').extract_first()
            visits = info.xpath('span[@style]/text()').extract_first().split(':')[1]
            tags = info.xpath('a[@title]/text()').extract()
            
            movie = dict(
                title=title, 
                visits=visits, 
                tags=tags, 
                releaseDate=releaseDate,
                description=description,
                doubanScore=doubanScore
                )
            
            yield movie
        
        page = response.css('div.pageturn')
        curr_page = int(page.css('b::text').extract_first())
        next_link = page.css('a.next::attr(href)').extract_first()
        
        if curr_page < 3 and next_link is not None:
            yield response.follow(next_link, callback=self.parse)

    def trim_title(self, title):
        lb = '【'
        if lb not in title:
            return title
        return self.re_ttlb.findall(title)[0].strip()